/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class MyContract extends Contract {

  /**
   * 
   * addCustomer
   *
   * When a customer signs up on the mobile up, new member listing is created on the blockchain.
   * @param customerId - the unique id to identify customer
   * @param firstName - users first name
   * @param surname - users surname
   * @param otherName - users other name
   * @param birthDate - string for date of birth
   * @param nationalIdNumber - users legally issued identification number
   * @param occupation - can be users current job or career title
   * @param gender - users preferred gender, can be male, female or undefined
   * @param region - users current location i.e Country
   * @param subRegion - users current sub-location usually City
   * @param Address - users postal or physical address
   * @param phoneNumber - users registered phone number
   * @param Email - users primary email address
   */

  async addCustomer(ctx, customerId, firstName, surname, otherName, birthDate, nationalIdNumber, 
    occupation, gender, region, subRegion, Address, phoneNumber, Email) {
    console.info('addCustomer invoked');

    //create object to hold details of our new csutomer
    let newCustomer = {};

    newCustomer.customerId = customerId;
    newCustomer.firstName = firstName;
    newCustomer.surname = surname;
    newCustomer.otherName = otherName;
    newCustomer.birthDate = birthDate;
    newCustomer.nationalIdNumber = nationalIdNumber;
    newCustomer.occupation = occupation;
    newCustomer.gender = gender;
    newCustomer.region = region;
    newCustomer.subRegion = subRegion;
    newCustomer.Address = Address;
    newCustomer.phoneNumber = phoneNumber;
    newCustomer.Email = Email;


    await ctx.stub.putState(id, Buffer.from(JSON.stringify(newCustomer)));
    console.info('updated ledger with key: ' + id + 'and value: ');
    console.info(JSON.stringify(newCustomer));
    return newCustomer;

  }

  async init(ctx) {
    console.info('init invoked');

  }


  /**
   * 
   * createContract
   * 
   * Transaction used when creating a contract for a newly added member. Will
   * record contract start date, cover duration, cover type, payment options, etc.  
   * Users then can use this contractId later to get more details from the
   * blockchain about their cover
   * @param contractId - the Id of the contract of cover to be created
   */
  async createContract(ctx, contractId, batchId, transactionId) {
    console.info('createContract called');
    if (contractId.length <= 0) {
      throw new Error('Please enter the contractId');
    }

    let coverContract = {};
    coverContract.contractId = contractId;

    //there needs to be a batch associated with the contract that is created
    let contractAsBytes = await ctx.stub.getState(batchId);
    if (!contractAsBytes || contractAsBytes.length === 0) {
      return new Error(`${batchId} batch does not exist`);
    }

    coverContract.batchId = batchId;
    coverContract.transactionId = transactionId;

    console.info('createContract called after transId');

    //get the first character of the contractId - this represents cover type
    let coverType = contractId.charAt(0);
    if (coverType.toLowerCase() === 'r') {
      coverContract.coverType = 'Repair';
    } else if (coverType.toLowerCase() === 't') {
      coverContract.coverType = 'Theft';
    } else {
      coverContract.coverType = 'Repair & Theft';
    }

    console.info('createContract called after coverType');

    //get the 2nd character of contractId - this represents the payment options prefferred for the contract
    let paymentOptions = contractId.charAt(1);
    if (paymentOptions.toLowerCase === 'm') {
      coverContract.paymentOptions = 'Monthly';
    } else if (paymentOptions.toLowerCase() === 'b') {
      coverContract.paymentOptions = 'Bi-Annually';
    } else {
      coverContract.paymentOptions = 'Annually';
    }

    console.info('createContract called after paymentOptions'); 

    //get the 3nd character of contractId - this represents the cover duration
    let coverDuration = contractId.charAt(2);
    if (coverDuration.toLowerCase() === 'y') {
      coverContract.coverDuration = '12 Months';
    }
    console.info('createContract called after after coverDuration');

    let dateStr = new Date();
    dateStr = dateStr.toString();
    coverContract.startDate = dateStr;
    console.info('createContract called after after date');

    coverContract.class = 'com.smartinsurance.createContract';

    console.log('contractId')
    console.log(contractId)

    await ctx.stub.putState(cupId, Buffer.from(JSON.stringify(coverContract)));
    console.info('updated ledger with key: ' + contractId + 'and value: ');
    console.info(JSON.stringify(coverContract));
    return coverContract;
  }

  /**
   * 
   * submitDeviceData
   * 
   * When a customer adds a contract cover to the blockchain.
   * This creates the contract asset on the blockchain.
   * @param ostype  - what ostype is the contract created for
   * @param location - address of contract user
   * @param phoneBrand - can be Samsung, Apple, Huawei, Sony etc
   * @param phoneModel - is the specific model produced by each phoneBrand
   * @param purchaseDate - date the device was bought
   * @param purchasePrice - amount of money device was paid for
   */

  // let response = await args.contract.submitTransaction(args.function
  //  args.ostype,args.location, args.phoneBrand, args.phoneModel,
  //  args.purchaseDate,args.purchasePrice);

  async submitDeviceData(ctx, ostype, location, phoneBrand, phoneModel,
    purchaseDate, purchasePrice) {
    console.info('submitDeviceData invoked');

    //TODO: 
    //do check to make sure the customer exists in the blockchain

    let coverDevice = {};
    // generate random batchId from Math.random function
    let deviceId = Math.random().toString(36).substring(3);
    coverDevice.ostype = ostype;
    coverDevice.location = location;
    coverDevice.phoneBrand = phoneBrand;
    coverDevice.phoneModel = phoneModel;
    coverDevice.purchaseDate = purchaseDate;
    coverDevice.purchasePrice = purchasePrice;

  
    await ctx.stub.putState(batchId, Buffer.from(JSON.stringify(coverDevice)));
    console.info('updated ledger with key: ' + deviceId + 'and value: ');
    console.info(JSON.stringify(coverDevice));
    return coverDevice;
  }

  /**
   * 
   * initClaim
   * 
   * A transaction which submits a claim request from the customer to insurer on the blockchain.
   * @param claimId - id of newly created claim
   * @param contractId - Id of the contract which covers details of customer
   * @param firstName - customer firstName as registered in the system
   * @param lastName - customer lastName as registered in the system
   * @param coverType - can be repair or theft & repair as registered in the system
   * @param coverPrice - Priced pay for the cover of submitted device
   * @param transactionId - transaction Id created when new contract was created
   * @param timestamp - when the transaction was submitted to the blockchain
   */

  async initClaim(ctx, claimId, contractId, firstName, lastName, coverType,
    coverPrice, transactionId, timestamp) {
    console.info('init claim invoked');

    //TODO: do if (batch exists) check

    //get batch identified bby batchId from the ledger
    // let contractAsBytes = await ctx.stub.getState(batchId);
    // let batchCoffee = JSON.parse(coffeeAsBytes);
    let contractAsBytes = await ctx.stub.getState(batchId);
    if (!contractAsBytes || contractAsBytes.length === 0) {
      return new Error(`${batchId} batch does not exist`);
    }
    let coverClaim = JSON.parse(contractAsBytes);
    //update our batch of coffee with the shipping details
    coverClaim.claimId = claimId;
    coverClaim.contractId = contractId;
    coverClaim.firstName = firstName;
    coverClaim.lastName = lastName;
    coverClaim.coverType = coverType;
    coverClaim.coverPrice = coverPrice;
    coverClaim.transactionId = transactionId;
    coverClaim.timrestamp = timestamp;
    
    //update the ledger with the new claim + owner details
    await ctx.stub.putState(batchId, Buffer.from(JSON.stringify(batchCoffee)));
    console.info('updated ledger with key: ' + batchId + 'and value: ');
    console.info(JSON.stringify(batchCoffee));
    return batchCoffee;

  }

  async query(ctx, key) {
    console.info('query by key ' + key);
    let returnAsBytes = await ctx.stub.getState(key);
    console.info(returnAsBytes)
    if (!returnAsBytes || returnAsBytes.length === 0) {
      return new Error(`${key} does not exist`);
    }
    let result = JSON.parse(returnAsBytes);
    console.info('result of getState: ');
    console.info(result);
    return JSON.stringify(result);
  }

  async deleteKey(ctx, key) {
    console.info('delete key: ' + key);
    let returnAsBytes = await ctx.stub.deleteState(key);
    console.info(returnAsBytes)
    if (!returnAsBytes || returnAsBytes.length === 0) {
      console.info('no bytes returned')
      return new Error(`successfully deleted key: ${key}`);
    }
    // let result = JSON.parse(returnAsBytes);
    // console.info('result of deleteState: ');
    // console.info(result);
    return JSON.stringify(returnAsBytes);
  }

  async queryAll(ctx) {

    let queryString = {
      "selector": {}
    }

    let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
    return queryResults;

  }

  async queryWithQueryString(ctx, queryString) {

    console.log("query String");
    console.log(JSON.stringify(queryString));

    let resultsIterator = await ctx.stub.getQueryResult(queryString);

    let allResults = [];

    while (true) {
      let res = await resultsIterator.next();

      if (res.value && res.value.value.toString()) {
        let jsonRes = {};

        console.log(res.value.value.toString('utf8'));

        jsonRes.Key = res.value.key;

        try {
          jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
        } catch (err) {
          console.log(err);
          jsonRes.Record = res.value.value.toString('utf8');
        }

        allResults.push(jsonRes);
      }
      if (res.done) {
        console.log('end of data');
        await resultsIterator.close();
        console.info(allResults);
        console.log(JSON.stringify(allResults));
        return JSON.stringify(allResults);
      }
    }
  }

}

module.exports = MyContract;