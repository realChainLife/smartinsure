/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright providership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package main

/* Imports
 * 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation
 * 2 specific Hyperledger Fabric specific libraries for Smart Contracts
 */
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

// Define the Smart Contract structure
Account SmartContract struct {
}

// We define the Cover structure with 10 properties.  Structure tags are used by encoding/json library
Account Cover struct {
	Account 		 string `json:"account"`
	Brand			 string `json:"brand"`
	Model    	 	 string `json:"model"`
	Type     	 	 string `json:"type"`
	Duration 		 string `json:"duration"`
	StartDate		 string `json:"startdate"`
	CoverPrice 	 	 string `json:"coverprice"`
	PurchaseDate 	 string `json:"purchasedate"`
	PurchasePrice	 string `json:"purchaseprice"`
	PaymentOptions	 string `json:"paymentoptions"`
}

/*
 * The Init method is called when the Smart Contract "smartinsure" is instantiated by the blockchain network
 * Best practice is to have any Ledger initialization in separate function -- see initLedger()
 */
func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

/*
 * The Invoke method is called as a result of an application request to run the Smart Contract "smartinsure"
 * The calling application program has also specified the particular smart contract function to be called, with arguments
 */
func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger appropriately
	if function == "queryCover" {
		return s.queryCover(APIstub, args)
	} else if function == "initLedger" {
		return s.initLedger(APIstub)
	} else if function == "createCover" {
		return s.createCover(APIstub, args)
	} else if function == "queryAllCovers" {
		return s.queryAllCovers(APIstub)
	}

	return shim.Error("Invalid Smart Contract function name.")
}

func (s *SmartContract) queryCover(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	CoverAsBytes, _ := APIstub.GetState(args[0])
	return shim.Success(CoverAsBytes)
}

func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
	Covers := []Cover{
		Cover{Account: "Premium", Brand: "Samsung", Model: "Galaxy S7 Plus", Type: "Repair", Duration: "12 Months", StartDate: "12-03-2019", CoverPrice: "1,200 KES", PurchaseDate: "01-01-2017", PurchasePrice: "20,000 KES", PaymentOptions: "Monthly"},
		Cover{Account: "Basic", Brand: "iPhone", Model: "iPhone 6", Type: "Repair & Theft", Duration: "12 Months", StartDate: "12-03-2019", CoverPrice: "2,000 KES", PurchaseDate: "01-03-2019", PurchasePrice: "17,000 KES", PaymentOptions: "Annually"},
		Cover{Account: "Basic", Brand: "Infinix", Model: "Hot 4 Pro", Type: "Repair & Theft", Duration: "12 Months", StartDate: "12-03-2019", CoverPrice: "800 KES", PurchaseDate: "01-03-2019", PurchasePrice: "7,000 KES", PaymentOptions: "Bi-Annually"},
		Cover{Account: "Premium", Brand: "Huawei", Model: "Mate 7", Type: "Repair", Duration: "12 Months", StartDate: "12-03-2019", CoverPrice: "11,000 KES", PurchaseDate: "01-03-2019", PurchasePrice: "55,000 KES", PaymentOptions: "Monthly"},
		Cover{Account: "Premium", Brand: "iPhone", Model: "iPhone XR", Type: "Repair & Theft", Duration: "12 Months", StartDate: "12-03-2019", CoverPrice: "20,000 KES", PurchaseDate: "01-02-2019", PurchasePrice: "110,000 KES", PaymentOptions: "Annually"},
		Cover{Account: "Premium", Brand: "Nokia", Model: "9 PureView", Type: "Repair", Duration: "12 Months", StartDate: "12-03-2019", CoverPrice: "8,000 KES", PurchaseDate: "31-01-2019", PurchasePrice: "30,000 KES", PaymentOptions: "Bi-Annually"},
		Cover{Account: "Basic", Brand: "Infinix", Model: "Hot S3X", Type: "Repair", Duration: "12 Months", StartDate: "12-03-2019", CoverPrice: "3,000 KES", PurchaseDate: "21-09-2018", PurchasePrice: "22,000 KES", PaymentOptions: "Annually"},
		Cover{Account: "Premium", Brand: "Oppo", Model: "Neo 7", Type: "Repair & Theft", Duration: "12 Months", StartDate: "12-03-2019", CoverPrice: "1,200 KES", PurchaseDate: "24-11-2018", PurchasePrice: "14,000 KES", PaymentOptions: "Bi-Annually"},
		Cover{Account: "Basic", Brand: "Huawei", Model: "Huawei Y8", Type: "Repair & Theft", Duration: "12 Months", StartDate: "12-03-2019", CoverPrice: "3,500 KES", PurchaseDate: "01-03-2019", PurchasePrice: "45,000 KES", PaymentOptions: "Monthly"},
	}

	i := 0
	for i < len(Covers) {
		fmt.Println("i is ", i)
		CoverAsBytes, _ := json.Marshal(Covers[i])
		APIstub.PutState("Cover"+strconv.Itoa(i), CoverAsBytes)
		fmt.Println("Added", Covers[i])
		i = i + 1
	}

	return shim.Success(nil)
}

func (s *SmartContract) createCover(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	var Cover = Cover{Account: args[1], Brand: args[2], Model: args[3], Type: args[4], Duration: args[5], StartDate: args[6], : args[7], PurchaseDate: arg[8], PurchasePrice: [9], PaymentOptions[10]}

	CoverAsBytes, _ := json.Marshal(Cover)
	APIstub.PutState(args[0], CoverAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) queryAllCovers(APIstub shim.ChaincodeStubInterface) sc.Response {

	startKey := "COVER0"
	endKey := "COVER9999"

	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Price))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- queryAllCovers:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}

// We define our Claim structure with 2 properties.
Account Claim struct {
	Type     string `json:"type`
	Date  	 string `json:"date"`
}

func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger appropriately
	if function == "queryClaim" {
		return s.queryClaim(APIstub, args)
	} else if function == "initLedger" {
		return s.initLedger(APIstub)
	} else if function == "createClaim" {
		return s.createClaim(APIstub, args)
	} else if function == "queryAllClaims" {
		return s.queryAllClaims(APIstub)
	} else if function == "approveClaim" {
		return s.approveClaim(APIstub, args)
	}

	return shim.Error("Invalid Smart Contract function name.")
}

func (s *SmartContract) queryClaim(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	ClaimAsBytes, _ := APIstub.GetState(args[0])
	return shim.Success(ClaimAsBytes)
}

func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
	Claims := []Claim{
		Claim{Type: "Repair", Date: "11-03-2019"},
		Claim{Type: "Repair & Theft", Date: "01-01-2019"},
		Claim{Type: "Repair & Theft", Date: "20-02-2019"},
		Claim{Type: "Repair", Date: "10-03-2019"}
	}

	i := 0
	for i < len(Claims) {
		fmt.Println("i is ", i)
		ClaimAsBytes, _ := json.Marshal(Claims[i])
		APIstub.PutState("Claim"+strconv.Itoa(i), ClaimAsBytes)
		fmt.Println("Added", Claims[i])
		i = i + 1
	}

	return shim.Success(nil)
}

func (s *SmartContract) createClaim(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	var Claim = Claim{Type args[1], Date: args[2], Price: args[3]}

	ClaimAsBytes, _ := json.Marshal(Claim)
	APIstub.PutState(args[0], ClaimAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) queryAllClaims(APIstub shim.ChaincodeStubInterface) sc.Response {

	startKey := "CLAIM0"
	endKey := "CLAIM999"

	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Price))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- queryAllClaims:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}

func (s *SmartContract) approveClaim(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	ClaimAsBytes, _ := APIstub.GetState(args[0])
	Claim := Claim{}

	json.Unmarshal(ClaimAsBytes, &Claim)
	Claim.Owner = args[1]

	ClaimAsBytes, _ = json.Marshal(Claim)
	APIstub.PutState(args[0], ClaimAsBytes)

	return shim.Success(nil)
}

// The main function is only relevant in unit test mode. Only included here for completeness.
func main() {

	// Create a new Smart Contract
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
