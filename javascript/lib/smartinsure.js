/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Track the creation of an insurance contract by one customer from an insurer
 * @param {com.smartinsurance.createContract} create - the contract to be created
 * @transaction
 */
async function createContract(create) {
    create.contract.owner = create.newOwner;
    let assetRegistry = await getAssetRegistry('com.smartinsurance.Contract');
    await assetRegistry.update(create.contract);
}

/**
 * Track the initialization of an insurance claim by one customer to an insurer
 * @param {com.smartinsurance.initClaim} claim - the claim to be initialized
 * @transaction
 */
async function initClaim(init) {
  init.claim.owner = init.newOwner;
  let assetRegistry = await getAssetRegistry('com.smartinsurance.Claim');
  await assetRegistry.update(init.claim);
}

/**
 * Track the initialization of an insurance claim by one customer to an insurer
 * @param {com.smartinsurance.queryContract} contract - the contract to be queried
 * @transaction
 */
async function queryContract(query) {
  query.contract.owner = query.newOwner;
  let assetRegistry = await getAssetRegistry('com.smartinsurance.Contract');
  await assetRegistry.update(query.contract);
}

/**
 * Track the initialization of an insurance claim by one customer to an insurer
 * @param {com.smartinsurance.queryClaim} claim - the contract to be queried
 * @transaction
 */
async function queryClaim(query) {
  query.claim.owner = query.newOwner;
  let assetRegistry = await getAssetRegistry('com.smartinsurance.Claim');
  await assetRegistry.update(query.claim);
}
