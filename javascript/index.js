/*
# Copyright Bismart Corp. All Rights Reserved, 2019.
#
# SPDX-License-Identifier: Apache-2.0
*/

'use strict';

const SmartInsure = require('./lib/smartinsure');

module.exports.SmartInsure = SmartInsure;
module.exports.contracts = [ SmartInsure ];
