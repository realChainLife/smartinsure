# SmartInsure by Bismart

This is a Javascript Chaincode Implementation on Hyperleger Fabric to deploy and execute Bismart's SmartInsure Contracts. 

The chaincode exposes the following methods

## Init
Called by HLF to initialize the chaincode.


## addCustomer
Called by HLF when a transaction is received. Dispatches to the functions below.


## initLedger
Initializes the state of the ledger for this chaincode


## createContract
Deploys a smart & legal insurance contract to the ledger

### Arguments
* (property) coverType, paymentOptions, coverDuration {...}: the identifier of the contract. Used on subsequent calls to executeSmartContract.
* Chaincode.query(method), a base-64 encoded SmartInsure archive.
* contractData the property parameters (as an arg) that parameterizes the contract as a valid instance.
* state, a JSON object as a string for the initial state of the contract. Must be a valid instance of the state model for the contract.
* queryCover, queryClaim - Executes a previously deployed Smart Contract, returning results and emitting events.

## License 

Hyperledger Project source code files are made available under the Apache
License, Version 2.0 (Apache-2.0), located in the [LICENSE](LICENSE) file.
Hyperledger Project documentation files are made available under the Creative
Commons Attribution 4.0 International License (CC-BY-4.0), available at http://creativecommons.org/licenses/by/4.0/.